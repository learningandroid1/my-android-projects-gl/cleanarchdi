package vezdeborg.truecode.cleanarch.data.api

import retrofit2.http.GET
import vezdeborg.truecode.cleanarch.data.model.UsefulActivityDto

interface GetUsefulActivityApi {
    @GET("api/activity")
    suspend fun getActivity(): UsefulActivityDto
}