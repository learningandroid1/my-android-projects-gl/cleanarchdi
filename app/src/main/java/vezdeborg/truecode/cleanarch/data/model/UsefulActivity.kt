package vezdeborg.truecode.cleanarch.data.model

interface UsefulActivity {
    val activity: String
}