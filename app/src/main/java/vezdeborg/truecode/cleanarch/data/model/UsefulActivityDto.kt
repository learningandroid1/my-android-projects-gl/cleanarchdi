package vezdeborg.truecode.cleanarch.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import vezdeborg.truecode.cleanarch.data.model.UsefulActivity

@JsonClass(generateAdapter = true)
class UsefulActivityDto(@Json(name = "activity") override val activity: String): UsefulActivity