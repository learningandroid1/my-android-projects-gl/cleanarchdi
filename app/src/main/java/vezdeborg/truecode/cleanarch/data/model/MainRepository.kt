package vezdeborg.truecode.cleanarch.data.model

import vezdeborg.truecode.cleanarch.data.api.RetrofitServices
import javax.inject.Inject

class MainRepository @Inject constructor(){
    suspend fun getActivityInfo(): UsefulActivity {
        return RetrofitServices.getUsefulActivityApi.getActivity()
    }
}