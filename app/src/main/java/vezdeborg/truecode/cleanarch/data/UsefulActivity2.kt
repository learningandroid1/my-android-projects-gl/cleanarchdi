package vezdeborg.truecode.cleanarch.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UsefulActivity2(@Json(name = "activity") val activity: String)
