package vezdeborg.truecode.cleanarch.domain

import vezdeborg.truecode.cleanarch.data.model.MainRepository
import vezdeborg.truecode.cleanarch.data.model.UsefulActivity
import javax.inject.Inject

class GetUsefulActivityUseCase @Inject constructor(private val mainRepository: MainRepository) {
    suspend fun execute(): UsefulActivity {
        return mainRepository.getActivityInfo()
    }
}