package vezdeborg.truecode.cleanarch.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import vezdeborg.truecode.cleanarch.data.api.RetrofitServices
import vezdeborg.truecode.cleanarch.domain.GetUsefulActivityUseCase
import javax.inject.Inject

class MainViewModel @Inject constructor(val getUsefulActivityUseCase: GetUsefulActivityUseCase): ViewModel() {

    private var _activityText = MutableStateFlow("What are you doing today?")
    val activityText = _activityText.asStateFlow()

    fun reloadUsefulActivity() {
        viewModelScope.launch {
            _activityText.value = getUsefulActivityUseCase.execute().activity
        }
    }
}